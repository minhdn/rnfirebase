import React, { Component } from 'react';
import * as firebase from 'firebase';
import Firebase from './includes/firebase/firebase';
import Home from './includes/views/home';
import Login from './includes/views/login';

import {
  AppRegistry,
  Text,
  View,
  Navigator,
} from 'react-native';

export default class MenuCreations extends Component {
  constructor(props) {
    super(props);
    Firebase.initialize();
    this.state = {
      userLoaded: false,
      initialView: null,
    }
  }

  componentWillMount() {
    const currentPage = this;
    firebase.auth().onAuthStateChanged((user) => {
      let initialView = user ? 'Home' : 'Login';
      currentPage.setState({
        userLoaded: true,
        initialView: initialView,
      });
    });
  }

  static renderScene(route, navigator) {
    switch (route.name) {
      case 'Home':
        return (<Home navigator={navigator}/>);
        break;
      case 'Login':
        return (<Login navigator={navigator}/>);
        break; 
    }
  }

  static configureScene(route) {
    if (route.sceneConfig) {
      return (route.sceneConfig);
    } else {
      return ({
        ...Navigator.SceneConfigs.FloatFromBottom,
        gestures: {}
      });
    }
  }

  render() {
    if (this.state.userLoaded) {
      return (
          <Navigator
              initialRoute={{name: this.state.initialView}}
              renderScene={MenuCreations.renderScene}
              configureScene={MenuCreations.configureScene}
          />);
    } else {
      return null;
    }
  }
}

AppRegistry.registerComponent('MenuCreations', () => MenuCreations);
