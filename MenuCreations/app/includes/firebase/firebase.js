import * as firebase from 'firebase';

class Firebase {
  static initialize() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDSToXEcoX_Sdz2IoKSGP8SW5acw7A9WO0',
      authDomain: 'menucreation-25afd.firebaseapp.com',
      databaseURL: 'https://menucreation-25afd.firebaseio.com',
      storageBucket: 'menucreation-25afd.appspot.com',
      messagingSenderId: '43958255116',
    });
  }
}
module.exports = Firebase;
